jQuery('.pv-pdf-gen').on('click', function (e) {
    e.preventDefault();
    document.querySelector('.pv-pdf-gen').innerHTML = "<span style='color:blue'>Updating... Please Wait.</span> ";
    var data = {};
    jQuery.post('/projects_pdf/pv/17396/?pdf=17396', data, function (response) {
        parsedResp = JSON.parse(response);
        if (parsedResp.status == 'success') {
            document.querySelector('.pv-pdf-gen').innerHTML = "<span style='color:green'>PV PDF Updated</span> ";
        } else {
            console.log(response);
            document.querySelector('.pv-pdf-gen').innerHTML = "<span style='color:red'>Updating ERROR</span> ";
        }
    });
    /* eslint-enable */
});

jQuery('.wind-pdf-gen').on('click', function (e) {
    e.preventDefault();
    document.querySelector('.wind-pdf-gen').innerHTML = "<span style='color:blue'>Updating... Please Wait.</span> ";
    var data = {};
    jQuery.post('/projects_pdf/wind/17395/?pdf=17395', data, function (response) {
        parsedResp = JSON.parse(response);
        console.log(parsedResp.status);
        if (parsedResp.status == 'success') {
            document.querySelector('.wind-pdf-gen').innerHTML = "<span style='color:green'>Wind PDF Updated</span> ";
        } else {
            console.log(response);
            document.querySelector('.wind-pdf-gen').innerHTML = "<span style='color:red'>Updating ERROR</span> ";
        }
    });
    /* eslint-enable */
});
