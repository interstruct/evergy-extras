jQuery('.map-pdf-gen').on('click', function (e) {
  e.preventDefault();
  document.querySelector('.map-pdf-gen').innerHTML = "<span style='color:blue'>Updating... Please Wait.</span> ";
  var data = {
    action: 'mapjson'
  };
  jQuery.post(ajaxurl, data, function (response) {
      parsedResp = JSON.parse(response);
      
    if (parsedResp.status == 'success') {
      document.querySelector('.map-pdf-gen').innerHTML = "<span style='color:green'>Map JSON Updated</span> ";
    } else {
        document.querySelector('.map-pdf-gen').innerHTML = "<span style='color:red'>Updating ERROR</span> ";
    }
  });
  /* eslint-enable */
});