<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

    <?php while (have_posts()) : the_post(); ?>
        <main class="main">
            <?php
            if (have_rows('page_builder')) {
                while (have_rows('page_builder')) {
                    the_row();
                    ?>
                    <div <?php qbees_the_component_conteiner(); ?>>
                            <?php get_template_part('template-parts/builder/' . get_row_layout() . '/' . get_row_layout()); ?>
                    </div>
                    <?php
                }
            }
            ?>
        </main>
    <?php endwhile; // end of the loop.  ?>
<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>