<?php
$header_logo = wp_get_attachment_image_src(get_field('header_logo', 'option'), 'full');
?>
<div class="grid">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
        </div>
    </div>
</div>
<div class="headers js-headers">
<header class="header">
    <div class="wrapper">
        <div class="row">
            <div class="header__logo col-dl-6 col-dl-push-4 col-tm-13">
                <div class="header__logo-in">
                    <div class="logo logo_header">
                        <a href="<?php echo icl_get_home_url() ?>" class="logo__in" title="To Homepage">
                            <img src="<?php echo $header_logo[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                        </a>
                    </div>
                </div>
            </div>
            <?php
            
            if (( $locations = get_nav_menu_locations() ) && isset($locations['primary'])) {
                $menu = get_term($locations['primary']);

                $menu_items = wp_get_nav_menu_items($menu->term_id);
            }


            if (count($menu_items) > 0) {
                ?>
                <div class="header__nav col-dl-10 col-dl-push-1">
                    <nav class="nav">
    <?php
    foreach ((array) $menu_items as $key => $menu_item) {
        $active_class = '';
        if ($menu_item->url == get_permalink()) {
            $active_class = 'is-active';
        }
        ?>
                            <a href="<?php echo $menu_item->url; ?>" class="nav__link <?php echo $active_class; ?> js-clear-storage" title="<?php echo strip_tags($menu_item->title); ?>"><?php echo $menu_item->title; ?></a>
                        <?php } ?>
                    </nav>
                </div>
                <div class="header__burger col-dl-10 col-dl-push-8 col-tm-3 col-tm-push-1">
                    <button class="burger js-burger">
                        <span class="burger__in">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
<?php } ?>
        </div>
    </div>
</header>
<header class="header-sticky">
    <div class="wrapper">
        <div class="row">
            <div class="header-sticky__text col-dl-6 col-dl-push-1 col-tm-13">
                <div class="header-sticky__text-in">
                    <h1><?php the_field('header_alt_text', 'option'); ?></h1>
                </div>
            </div>
<?php if (count($menu_items) > 0) { ?>
                <div class="header-sticky__nav col-dl-10 col-dl-push-1">
                    <nav class="nav">
    <?php
    foreach ((array) $menu_items as $key => $menu_item) {
        $active_class = '';
        if ($menu_item->url == get_permalink()) {
            $active_class = 'is-active';
        }
        ?>
                            <a href="<?php echo $menu_item->url; ?>" class="nav__link <?php echo $active_class; ?> js-clear-storage" title="<?php echo strip_tags($menu_item->title); ?>"><?php echo $menu_item->title; ?></a>
                        <?php } ?>
                    </nav>
                </div>
                <div class="header-sticky__burger col-dl-10 col-dl-push-8 col-tm-3 col-tm-push-1">
                    <button class="burger burger_sticky js-burger">
                        <span class="burger__in">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
<?php } ?>
        </div>
    </div>
</header>
</div>