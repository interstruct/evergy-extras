<?php
$ImagesArr = array();
$ImagesArr[0] = wp_get_attachment_image_src(get_sub_field('item1_image'), 'full');
$ImagesArr[1] = wp_get_attachment_image_src(get_sub_field('item2_image'), 'full');
$ImagesArr[2] = wp_get_attachment_image_src(get_sub_field('item3_image'), 'full');
$buttonsArr = array();
$buttonsArr[0] = get_sub_field('item1_button');
$buttonsArr[1] = get_sub_field('item2_button');
$buttonsArr[2] = get_sub_field('item3_button');
?>

<div class="accordion-images">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-16 col-dl-push-7 col-dm-push-8 col-ds-push-7">
                <div class="accordion-images__in js-parallax">
                    <div class="accordion-images__img js-accordion-img">
                        <img src="<?php echo $ImagesArr[0][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                    </div>
                    <div class="accordion-images__img js-accordion-img">
                        <img src="<?php echo $ImagesArr[1][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                    </div>
                    <div class="accordion-images__img js-accordion-img">
                        <img src="<?php echo $ImagesArr[2][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="accordion js-accordion">
    <div class="wrapper">
        <div class="row">
            <div class="accordion__stateicon col-tl-1">
                <svg class="state-icon state-icon_lg js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 32" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M2,32h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,31.1,0.9,32,2,32z" />
                        <path d="M2,25h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,24.1,0.9,25,2,25z" />
                        <path d="M2,18h6c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,17.1,0.9,18,2,18z" />
                        <path d="M0,2L0,2c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,0,0,0.9,0,2z" />
                        <path d="M0,9L0,9c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,7,0,7.9,0,9z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_md js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17h7c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-7C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h19C21.3,3,22,2.3,22,1.5l0,0C22,0.7,21.3,0,20.5,0h-19
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h19c0.8,0,1.5-0.7,1.5-1.5l0,0C22,7.7,21.3,7,20.5,7h-19
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_sm js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17H4c0.8,0,1.5-0.7,1.5-1.5l0,0C5.5,14.7,4.8,14,4,14H1.5C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h9C11.3,3,12,2.3,12,1.5l0,0C12,0.7,11.3,0,10.5,0h-9
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h9c0.8,0,1.5-0.7,1.5-1.5l0,0C12,7.7,11.3,7,10.5,7h-9
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
            </div>
            <div class="col-dl-7 col-ds-8 col-tl-15">
                <div class="accordion__title js-accordion-title">
                    <h2><span>independent</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion__in wrapper">
        <div class="accordion__content js-accordion-content row">
            <div class="accordion__content-in col-dl-6 col-dxl-8 col-dl-push-1 col-dm-7 col-tl-11 col-tl-push-2 col-ts-14 col-ml-13 col-ml-push-1 col-mm-15">
                <p><?php the_sub_field('item1_text'); ?></p>
                <img src="<?php echo $ImagesArr[0][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                <?php if (!empty($buttonsArr[0])) { ?>
                    <a href="<?php echo $buttonsArr[0]['url']; ?>" target="<?php echo $buttonsArr[0]['target']; ?>" class="button button_primary" title="<?php echo $buttonsArr[0]['title']; ?>"><span><?php echo $buttonsArr[0]['title']; ?></span></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="accordion js-accordion">
    <div class="wrapper">
        <div class="row">
            <div class="accordion__stateicon col-tl-1">
                <svg class="state-icon state-icon_lg js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 32" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M2,32h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,31.1,0.9,32,2,32z" />
                        <path d="M2,25h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,24.1,0.9,25,2,25z" />
                        <path d="M2,18h6c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,17.1,0.9,18,2,18z" />
                        <path d="M0,2L0,2c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,0,0,0.9,0,2z" />
                        <path d="M0,9L0,9c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,7,0,7.9,0,9z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_md js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17h7c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-7C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h19C21.3,3,22,2.3,22,1.5l0,0C22,0.7,21.3,0,20.5,0h-19
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h19c0.8,0,1.5-0.7,1.5-1.5l0,0C22,7.7,21.3,7,20.5,7h-19
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_sm js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17H4c0.8,0,1.5-0.7,1.5-1.5l0,0C5.5,14.7,4.8,14,4,14H1.5C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h9C11.3,3,12,2.3,12,1.5l0,0C12,0.7,11.3,0,10.5,0h-9
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h9c0.8,0,1.5-0.7,1.5-1.5l0,0C12,7.7,11.3,7,10.5,7h-9
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
            </div>
            <div class="col-dl-7 col-ds-8 col-tl-15">
                <div class="accordion__title js-accordion-title">
                    <h2><span>technical experts</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion__in wrapper">
        <div class="accordion__content js-accordion-content row">
            <div class="accordion__content-in col-dl-6 col-dxl-7 col-dl-push-1 col-tl-11 col-tl-push-2 col-ts-14 col-ml-13 col-ml-push-1 col-mm-15">
                <p><?php the_sub_field('item2_text'); ?></p>
                <img src="<?php echo $ImagesArr[1][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                <?php if (!empty($buttonsArr[1])) { ?>
                    <a href="<?php echo $buttonsArr[1]['url']; ?>" target="<?php echo $buttonsArr[1]['target']; ?>" class="button button_primary" title="<?php echo $buttonsArr[1]['title']; ?>"><span><?php echo $buttonsArr[1]['title']; ?></span></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="accordion js-accordion">
    <div class="wrapper">
        <div class="row">
            <div class="accordion__stateicon col-tl-1">
                <svg class="state-icon state-icon_lg js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 32" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M2,32h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,31.1,0.9,32,2,32z" />
                        <path d="M2,25h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,24.1,0.9,25,2,25z" />
                        <path d="M2,18h6c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,17.1,0.9,18,2,18z" />
                        <path d="M0,2L0,2c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,0,0,0.9,0,2z" />
                        <path d="M0,9L0,9c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,7,0,7.9,0,9z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_md js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17h7c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-7C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h19C21.3,3,22,2.3,22,1.5l0,0C22,0.7,21.3,0,20.5,0h-19
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h19c0.8,0,1.5-0.7,1.5-1.5l0,0C22,7.7,21.3,7,20.5,7h-19
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
                <svg class="state-icon state-icon_sm js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 31" shape-rendering="geometricPrecision">
                    <g fill="#EC6B06">
                        <path d="M1.5,31h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,28,0,28.7,0,29.5l0,0
                              C0,30.3,0.7,31,1.5,31z" />
                        <path d="M1.5,24h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,21,0,21.7,0,22.5l0,0
                              C0,23.3,0.7,24,1.5,24z" />
                        <path d="M1.5,17H4c0.8,0,1.5-0.7,1.5-1.5l0,0C5.5,14.7,4.8,14,4,14H1.5C0.7,14,0,14.7,0,15.5l0,0
                              C0,16.3,0.7,17,1.5,17z" />
                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h9C11.3,3,12,2.3,12,1.5l0,0C12,0.7,11.3,0,10.5,0h-9
                              C0.7,0,0,0.7,0,1.5z" />
                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h9c0.8,0,1.5-0.7,1.5-1.5l0,0C12,7.7,11.3,7,10.5,7h-9
                              C0.7,7,0,7.7,0,8.5z" />
                    </g>
                </svg>
            </div>
            <div class="col-dl-7 col-ds-8 col-tl-15">
                <div class="accordion__title js-accordion-title">
                    <h2><span>for renewable</span><br /><span>energy</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion__in wrapper">
        <div class="accordion__content js-accordion-content row">
            <div class="accordion__content-in col-dl-6 col-dxl-7 col-dl-push-1 col-tl-11 col-tl-push-2 col-ts-14 col-ml-13 col-ml-push-1 col-mm-15">
                <p><?php the_sub_field('item3_text'); ?></p>
                <img src="<?php echo $ImagesArr[2][0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                <?php if (!empty($buttonsArr[2])) { ?>
                    <a href="<?php echo $buttonsArr[2]['url']; ?>" target="<?php echo $buttonsArr[2]['target']; ?>" class="button button_primary" title="<?php echo $buttonsArr[2]['title']; ?>"><span><?php echo $buttonsArr[2]['title']; ?></span></a>
                        <?php } ?>
            </div>
        </div>
    </div>
</div>

