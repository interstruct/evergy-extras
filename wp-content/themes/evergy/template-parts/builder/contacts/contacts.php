<div class="wrapper">
    <?php if (get_sub_field('headline') != '' || get_sub_field('copy_text') != '') { ?>
        <div class="row">
            <div class="col-dl-12 col-dl-push-4  col-tl-15 col-tl-push-4 col-ml-push-1">
                <div class="contacts__content">
                    <?php if (get_sub_field('headline') != '') { ?>
                        <h3><?php the_sub_field('headline'); ?></h3>
                    <?php } ?>
                    <?php
                    if (get_sub_field('copy_text') != '') {
                        the_sub_field('copy_text');
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-dl-16 col-dl-push-4 col-ds-16 col-ds-push-4 col-tl-push-4 col-tm-15 col-ml-push-1 ">
            <div class=" contacts__in">
                <?php
                if (have_rows('contact_info_blocks')) {
                    while (have_rows('contact_info_blocks')) {
                        the_row();
                        if (get_row_index() == 1) {

                            $blockImage = wp_get_attachment_image_src(get_sub_field('image'), 'full');
                            ?>

                            <div class="contacts__item contacts__item--big">
                                <div class="contact-box">
                                    <div class="contact-box__img ">
                                        <img src="<?php echo $blockImage[0]; ?>" alt="<?php bloginfo('name')?>">
                                    </div>
                                    <h6 class="contact-box__title"><?php the_sub_field('title'); ?></h6>
                                    <div class="contact-box__content">

                                        <?php if (get_sub_field('address_line_1') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_1'); ?></p>
                                        <?php } ?>

                                        <?php if (get_sub_field('address_line_2') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_2'); ?></p>
                                        <?php } ?>
                                        <?php if (get_sub_field('address_line_3') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_3'); ?></p>
                                        <?php } ?>

                                        <div class="contact-box__links-wrap">
                                            <?php if (get_sub_field('phone_number') != '') { ?>
                                                T <a href="tell:<?php echo get_sub_field('phone_number'); ?>" title="Phone Number"><?php echo get_sub_field('phone_number'); ?></a><br />
                                            <?php } ?>
                                            <?php if (get_sub_field('fax_number') != '') { ?>
                                                F <a href="tell:<?php echo get_sub_field('fax_number'); ?>" title="Fax Number"><?php echo get_sub_field('fax_number'); ?></a><br />
                                            <?php } ?>
                                            <?php if (get_sub_field('email') != '') { ?>
                                                <p>E <a href="mailto:<?php echo get_sub_field('email'); ?>" title="Email"><?php echo get_sub_field('email'); ?></a></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                            break;
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-dl-16 col-dl-push-4 col-ds-16 col-ds-push-4 col-tl-push-4 col-tm-15 col-ml-push-1 ">
            <div class=" contacts__in">
                <?php
                if (have_rows('contact_info_blocks')) {
                    while (have_rows('contact_info_blocks')) {
                        the_row();
                        if (get_row_index() > 1) {

                            $blockImage = wp_get_attachment_image_src(get_sub_field('image'), 'full');
                            ?>
                            <div class="contacts__item ">
                                <div class="contact-box">
                                    <div class="contact-box__img ">
                                        <img src="<?php echo $blockImage[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">
                                    </div>
                                    <h6 class="contact-box__title"><?php the_sub_field('title'); ?></h6>
                                    <div class="contact-box__content">

                                        <?php if (get_sub_field('address_line_1') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_1'); ?></p>
                                        <?php } ?>

                                        <?php if (get_sub_field('address_line_2') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_2'); ?></p>
                                        <?php } ?>
                                        <?php if (get_sub_field('address_line_3') != '') { ?>
                                            <p><?php echo get_sub_field('address_line_3'); ?></p>
                                        <?php } ?>

                                        <div class="contact-box__links-wrap">
                                            <?php if (get_sub_field('phone_number') != '') { ?>
                                                T <a href="tell:<?php echo get_sub_field('phone_number'); ?>" title="Phone Number"><?php echo get_sub_field('phone_number'); ?></a><br />
                                            <?php } ?>
                                            <?php if (get_sub_field('fax_number') != '') { ?>
                                                F <a href="tell:<?php echo get_sub_field('fax_number'); ?>" title="Fax Number"><?php echo get_sub_field('fax_number'); ?></a><br />
                                            <?php } ?>
                                            <?php if (get_sub_field('email') != '') { ?>
                                                <p>E <a href="mailto:<?php echo get_sub_field('email'); ?>" title="Email"><?php echo get_sub_field('email'); ?></a></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>