<div class="projects-pattern js-projects" data-json="<?php echo get_stylesheet_directory_uri(); ?>/assets/projects.json" data-wp-request="true">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-12 col-dl-push-4 col-ds-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
                <div class="projects-pattern__content">
                    <h3><?php the_sub_field('headline'); ?></h3>
                    <p><?php the_sub_field('copy_text'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="projects-pattern__filter col-dl-4 col-dl-push-1 col-ml-16 col-ml-push-0">
                <div class="filter-box js-filter-box">
                    <div class="filter-box__col">
                        <div class="filter-box__name">filter<br />cases<br />by</div>
                        <div class="filter-box__list">
                            <a href="#" class="filter-box__case js-filter-btn" data-filter-type="PV" title="Show PV">
                                <svg class="icon icon-filter-pv">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-pv"></use>
                                </svg>
                            </a>
                            <a href="#" class="filter-box__case js-filter-btn" data-filter-type="Wind" title="Show Wind">
                                <svg class="icon icon-filter-w">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-w"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="filter-box__col">
                        <div class="filter-box__name">download<br />track<br />record</div>
                        <div class="filter-box__list">
                            <a href="<?php echo get_option('pv-pdf-url'); ?>" class="filter-box__load" download  title="Download PV track record">
                                <svg class="icon icon-download-pv">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-pv"></use>
                                </svg>
                            </a>
                            <a href="<?php echo get_option('wind-pdf-url'); ?>" class="filter-box__load" download  title="Download Wind track record">
                                <svg class="icon icon-download-w">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-w"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-dl-12 col-ml-15 col-ml-push-1">
                <div class="projects-pattern__list-wrapper js-projects-list-wrapper">
                    <div class="projects-pattern__list js-projects-list">
                        <?php
                        $projects_first_package = projectjson_callback("yes", 1, get_field('column_6'));
                        $load_more_btn = "";
                        foreach ($projects_first_package as $item) {
                            if ($load_more_btn=="" && $item['last']!=0){
                                $load_more_btn = 'hidden';
                            }
                            if ($item['type'] != 'spec') {
                                ?>
                                <div class="projects-pattern__item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                    <a href="<?php echo $item['url']; ?>" class="project js-projects-storage-btn" title="Read more about <?php echo $item['title']; ?>">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <?php if ($item['type'] == 'Wind'){ ?>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 322 177" style="enable-background:new 0 0 322 177;" xml:space="preserve">
            <g id="icons_x2F_cases_x2F_w">
            	<path id="W" d="M64.8,140H52.4L26.8,41.6h9.5l9.2,36.5l13.3,52.7c6.5-23.3,11.5-40.8,15-52.7l10.2-35h10.7l10.9,35.2l16.4,53.1
            		c5.4-24.9,9.2-42.6,11.5-53l8.2-36.8h9.1L128.5,140H116l-9.8-31.1l-16.6-54c-3.7,13.4-8.9,31.4-15.6,54L64.8,140z"></path>
            </g>
            </svg>
                                                <?php } else if ($item['type'] == 'PV'){?>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 322 177" enable-background="new 0 0 322 177" xml:space="preserve">
            <g id="icons_x2F_cases_x2F_pv">
            	<path id="PV" d="M57.9,102.6H41.7V140h-9.1V41.6h23.7c11.5,0,20.5,2.9,27.1,8.7s9.9,13.4,9.9,22.8c0,8.2-3.2,15.1-9.6,20.9
            		C77.3,99.8,68.7,102.6,57.9,102.6z M57.4,49.6H41.7v45H59c7.7,0,13.8-2,18.3-6.1c4.5-4.1,6.8-9.2,6.8-15.3c0-7.2-2.3-12.9-7-17.2
            		C72.4,51.7,65.8,49.6,57.4,49.6z M151.5,140h-12.4l-33.4-98.4h9.8l13,39l16.9,51.4l17.5-51.4l13.4-39h9.8L151.5,140z"></path>
            </g>
            </svg>
                                                <?php } ?>
                                            </span>
                                            <span class="project__text"><?php echo $item['desc']; ?></span>
                                            <span class="project__title"><?php echo $item['title']; ?></span>
                                            <div class="project__link-wrapper">
                                                <span class="project__link">Read more</span>
                                            </div>
                                        </span>
                                    </a>
                                </div>
                            <?php } else { ?>
                                <div class="projects-pattern__item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                    <a href="<?php echo get_field('map_page', 'option'); ?>" class="project project_special" title="Projects Map">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <img src="<?php echo $item['img']; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">
                                            </span>
                                            <span class="project__title project__title_special"><?php echo $item['title']; ?></span>
                                        </span>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-dl-16 col-dl-push-4 col-ml-15 col-ml-push-1">
                <div class="projects-pattern__more-wrapper">
                    <button class="projects-pattern__more-btn <?php echo $load_more_btn; ?> js-more-btn">
                        <svg class="icon icon-load-more">
                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-load-more"></use>
                        </svg>
                        <strong>load more</strong>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

