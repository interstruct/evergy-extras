<div class="wrapper">
    <div class="row">
        <div class="col-dl-12 col-dl-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
            <?php
            if (have_rows('info_blocks')) {
                while (have_rows('info_blocks')) {
                    the_row();
                    ?>

                    <?php if (get_sub_field('block_type') == 'default') { ?>
                        <div class="intro-text__in">
                            <?php the_sub_field('text'); ?>
                            <?php
                            if (have_rows('buttons')) {
                                while (have_rows('buttons')) {
                                    the_row();

                                    $blockButton = get_sub_field('button');
                                    if (get_sub_field('scroll_on_target_page')) {
                                        $scrollto = "js-scroll-other-page-link";
                                    } else {
                                        $scrollto = "";
                                    }
                                    ?>
                                    <a href="<?php echo $blockButton['url']; ?>" target="<?php echo $blockButton['target']; ?>" class="button button_primary <?php echo $scrollto; ?>" title="<?php echo $blockButton['title']; ?>"><span><?php echo $blockButton['title']; ?></span></a>
                                    <?php
                                }
                            } else {
                                $blockButton = '';
                            }
                            ?>
                        </div>
                    <?php } else if (get_sub_field('block_type') == 'small') { ?>
                        <div class="intro-text__in intro-text__in_sm">
                            <?php if (!empty(get_sub_field('headline'))) { ?>
                                <h6><?php the_sub_field('headline'); ?></h6>
                            <?php } ?>
                            <?php the_sub_field('text'); ?>
                            <?php
                            if (have_rows('buttons')) {
                                while (have_rows('buttons')) {
                                    the_row();

                                    $blockButton = get_sub_field('button');
                                    if (get_sub_field('scroll_on_target_page')) {
                                        $scrollto = "js-scroll-other-page-link";
                                    } else {
                                        $scrollto = "";
                                    }
                                    ?>
                                    <a href="<?php echo $blockButton['url']; ?>" target="<?php echo $blockButton['target']; ?>" class="button button_primary <?php echo $scrollto; ?>" title="<?php echo $blockButton['title']; ?>"><span><?php echo $blockButton['title']; ?></span></a>
                                            <?php
                                        }
                                    } else {
                                        $blockButton = '';
                                    }
                                    ?>
                        </div>
                    <?php } ?>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
