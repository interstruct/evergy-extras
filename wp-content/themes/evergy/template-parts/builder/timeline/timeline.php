<div class="wrapper">
    <div class="row">
        <div class="col-dl-12 col-dl-push-4 col-ds-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
            <div class="timeline__content">
                <?php if (get_sub_field('headline') != '') { ?>
                    <h3><?php the_sub_field('headline'); ?></h3>
                <?php } ?>
                <?php if (get_sub_field('copy_text') != '') { ?>
                    <?php the_sub_field('copy_text'); ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php
if (have_rows('time_periods')) {
    while (have_rows('time_periods')) {
        the_row();
        ?>

        <div class="timeline__item js-timeline">
            <div class="wrapper">
                <div class="row">
                    <div class="col-dl-14 col-dl-push-4 col-tl-16 col-ml-15 col-ml-push-1">
                        <div class="timeline-box">
                            <div class="timeline-box__title">
                                <svg class="state-icon state-icon_lg js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 32" shape-rendering="geometricPrecision">
                                    <g fill="#EC6B06">
                                        <path d="M2,32h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,31.1,0.9,32,2,32z" />
                                        <path d="M2,25h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,24.1,0.9,25,2,25z" />
                                        <path d="M2,18h6c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,17.1,0.9,18,2,18z" />
                                        <path d="M0,2L0,2c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,0,0,0.9,0,2z" />
                                        <path d="M0,9L0,9c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,7,0,7.9,0,9z" />
                                    </g>
                                </svg>
                                <svg class="state-icon state-icon_md js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 31" shape-rendering="geometricPrecision">
                                    <g fill="#EC6B06">
                                        <path d="M1.5,31h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,28,0,28.7,0,29.5l0,0
                                              C0,30.3,0.7,31,1.5,31z" />
                                        <path d="M1.5,24h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,21,0,21.7,0,22.5l0,0
                                              C0,23.3,0.7,24,1.5,24z" />
                                        <path d="M1.5,17h7c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-7C0.7,14,0,14.7,0,15.5l0,0
                                              C0,16.3,0.7,17,1.5,17z" />
                                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h19C21.3,3,22,2.3,22,1.5l0,0C22,0.7,21.3,0,20.5,0h-19
                                              C0.7,0,0,0.7,0,1.5z" />
                                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h19c0.8,0,1.5-0.7,1.5-1.5l0,0C22,7.7,21.3,7,20.5,7h-19
                                              C0.7,7,0,7.7,0,8.5z" />
                                    </g>
                                </svg>
                                <svg class="state-icon state-icon_sm js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 31" shape-rendering="geometricPrecision">
                                    <g fill="#EC6B06">
                                        <path d="M1.5,31h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,28,0,28.7,0,29.5l0,0
                                              C0,30.3,0.7,31,1.5,31z" />
                                        <path d="M1.5,24h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,21,0,21.7,0,22.5l0,0
                                              C0,23.3,0.7,24,1.5,24z" />
                                        <path d="M1.5,17H4c0.8,0,1.5-0.7,1.5-1.5l0,0C5.5,14.7,4.8,14,4,14H1.5C0.7,14,0,14.7,0,15.5l0,0
                                              C0,16.3,0.7,17,1.5,17z" />
                                        <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h9C11.3,3,12,2.3,12,1.5l0,0C12,0.7,11.3,0,10.5,0h-9
                                              C0.7,0,0,0.7,0,1.5z" />
                                        <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h9c0.8,0,1.5-0.7,1.5-1.5l0,0C12,7.7,11.3,7,10.5,7h-9
                                              C0.7,7,0,7.7,0,8.5z" />
                                    </g>
                                </svg>
                                <h2 class="js-timeline-title"><?php the_sub_field('year'); ?></h2>
                            </div>
                            <div class="timeline-box__in">
                                <div class="timeline-box__content js-timeline-content">
                                    <div class="timeline-box__content-in">
                                        <?php
                                        if (have_rows('story')) {
                                            while (have_rows('story')) {
                                                the_row();
                                                ?>

                                                <?php if (get_sub_field('story_content_type') == 'default') { ?>

                                                    <div class="timeline-box__text">
                                                        <h6><?php the_sub_field('headline'); ?></h6>
                                                        <?php if (get_sub_field('text') != "") { ?>
                                                            <p><?php the_sub_field('text'); ?></p>
                                                        <?php } ?>
                                                    </div>

                                                    <?php
                                                    if (get_sub_field('button_type') == 'custom') {
                                                        if (!empty(get_sub_field('button'))) {
                                                            $storyButton = get_sub_field('button');
                                                            ?>
                                                            <div class="timeline-box__button">
                                                                <a href="<?php echo $storyButton['url']; ?>" target="<?php echo $storyButton['target']; ?>" class="button button_primary <?php echo $storyButton['target'] != "_blank" ? "js-timeline-storage-btn" : ""; ?>" title="<?php echo $storyButton['title']; ?>"><span><?php echo $storyButton['title']; ?></span></a>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else if (get_sub_field('button_type') == 'meet') {
                                                        $expertObj = get_sub_field('meet_expert');
                                                        ?>
                                                        <div class="timeline-box__button">
                                                            <?php $expertName = explode(" ", trim(get_field('name', $expertObj->ID))); ?>
                                                            <a href="" data-modal-id="<?php echo $expertObj->ID; ?>" class="button button_primary" title="meet <?php echo $expertName[0]; ?>"><span>meet <?php echo $expertName[0]; ?></span></a>
                                                        </div>
                                                    <?php }
                                                    ?>

                                                    <?php
                                                } else if (get_sub_field('story_content_type') == 'quote') {
                                                    $expertPhotos = get_field('images', get_field('expert', get_sub_field('quote')));
                                                    $quoteArr['slide-image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                                                    $quoteArr['slide-image-caption-1'] = get_field('name', get_field('expert', get_sub_field('quote')));
                                                    $quoteArr['slide-image-caption-2'] = get_field('hobby', get_field('expert', get_sub_field('quote')));
                                                    $quoteArr['slide-image-caption-2-1'] = get_field('working_evergy_since', get_field('expert', get_sub_field('quote')));
                                                    $quoteArr['slide-text-1'] = get_field('quote', get_sub_field('quote'));
                                                    $quoteArr['slide-text-2'] = get_field('translation', get_sub_field('quote'));
                                                    $quotePhoto = wp_get_attachment_image_src($quoteArr['slide-image'], 'full');
                                                    ?>

                                                    <div class="timeline-quote">
                                                        <div class="timeline-quote__pic">
                                                            <img src="<?php echo $quotePhoto[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>" />
                                                            <?php if (!empty($quoteArr['slide-image-caption-1']) && !empty($quoteArr['slide-image-caption-1'])) { ?>
                                                                <figcaption>
                                                                    <?php if (!empty($quoteArr['slide-image-caption-1'])) { ?>
                                                                        <span class="timeline-quote__name"><?php echo $quoteArr['slide-image-caption-1']; ?></span>
                                                                    <?php } ?>
                                                                    <?php if (!empty($quoteArr['slide-image-caption-2'])) { ?>
                                                                        <span class="timeline-quote__desc"><?php
                                                                            echo trim($quoteArr['slide-image-caption-2']);
                                                                            if (!empty($quoteArr['slide-image-caption-2-1'])) {
                                                                                echo ", " . $quoteArr['slide-image-caption-2-1'];
                                                                            }
                                                                            ?></span>
                                                                    <?php } ?>
                                                                </figcaption>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="timeline-quote__text">
                                                            <blockquote><?php echo $quoteArr['slide-text-1']; ?></blockquote>
                                                            <div class="timeline-quote__translation"><?php echo $quoteArr['slide-text-2']; ?></div>
                                                        </div>
                                                    </div>
                                                    <?php if (get_sub_field('meet_expert_button')) { ?>
                                                        <div class="timeline-box__button">
                                                            <?php $expertName = explode(" ", trim(get_field('name', get_field('expert', get_sub_field('quote'))))); ?>
                                                            <a href="" data-quote-id="<?php echo get_sub_field('quote'); ?>" data-modal-id="<?php echo get_field('expert', get_sub_field('quote')); ?>" class="button button_primary" title="meet <?php echo $expertName[0]; ?>"><span>meet <?php echo $expertName[0]; ?></span></a>
                                                        </div>
                                                    <?php } ?>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>