<div class="downloads__inner">
    <div class="wrapper">
        <?php
        $downloadsArr = array();
        $filesIDArr = array();

        if (get_sub_field('choose_by_category')) {

            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'downlodable_category',
                        'field' => 'term_id',
                        'terms' => get_sub_field('category'),
                    ),
                ),
                'post_type' => 'downloadable_files',
                'numberposts' => -1,
                'post_status' => 'publish',
                'suppress_filters' => 0
            );
            $filesDataArr = get_posts($args);
            foreach ($filesDataArr as $fileData) {
                $filesIDArr[] = $fileData->ID;
            }
        } else {
            $filesIDArr = get_sub_field('files');
        }

        for ($i = 0; $i < count($filesIDArr); $i++) {
            $downloadsArr[$i]['title'] = get_field('title', $filesIDArr[$i]);
            $downloadsArr[$i]['year'] = get_field('year', $filesIDArr[$i]);
            $downloadsArr[$i]['icon_id'] = get_field('icon', $filesIDArr[$i]);
            $downloadsArr[$i]['file_src'] = get_field('file_for_download', $filesIDArr[$i]);
        }

        ?>

        <div class="row">
            <div class="col-dl-16 col-dl-push-4  col-ml-15 col-ml-push-1">
                <div class="downloads__list">
                    <?php
                    foreach ($downloadsArr as $downloadItem) {
                        $downloadItemIcon = wp_get_attachment_image_src($downloadItem['icon_id'], 'full');
                        ?>
                        <div class="downloads__item">
                            <a href="<?php echo $downloadItem['file_src']; ?>" class="download" download title="Download <?php echo $downloadItem['title']; ?>">
                                <span class="download__in">
                                    <span class="download__get-icon">
                                        <i>
                                            <svg class="icon icon-icon-download-general">
                                            <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-icon-download-general"></use>
                                            </svg>
                                        </i>
                                    </span>
                                    <span class="download__img">
                                        <img src="<?php echo $downloadItemIcon[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>"/>
                                    </span>
                                    <span class="download__text-wrap">
                                        <span class="download__title"><?php echo $downloadItem['title']; ?></span>
                                        <?php if ($downloadItem['year']!=''){?>
                                        <span class="download__year"><?php echo $downloadItem['year']; ?></span>
                                        <?php } ?>
                                    </span>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
