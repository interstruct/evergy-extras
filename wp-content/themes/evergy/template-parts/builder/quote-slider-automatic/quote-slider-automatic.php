<div class="quote-slider__in swiper-container js-quote-slider">
    <div class="swiper-wrapper">
        <?php
        $quoteTerm = get_sub_field('type');

        $args = array(
            'post_type' => 'quotes_and_news',
            'orderby' => 'rand',
            'tax_query' => array(
                array(
                    'taxonomy' => 'q_n_category',
                    'field' => 'term_id',
                    'terms' => intval($quoteTerm),
                ),
            ),
            'fields' => 'ids',
            'posts_per_page' => 1,
        );
        $slidesIDs = new WP_Query($args);

        wp_reset_query();

        $slidesArr = array();
        $popupsArr = array();
        $sliderCount = 0;
        foreach ($slidesIDs->posts AS $slideID) {
            $sliderCount++;

            $slideArr = array(
                "slide-image" => "",
                "slide-image-caption-1" => "",
                "slide-image-caption-2" => "",
                "slide-image-caption-2-1" => "",
                "slide-text-1" => "",
                "slide-text-2" => "",
                "slide-link-1" => "",
                "slide-link-2" => "",
            );

            $popupArr = array(
                "number" => 1,
                "name" => "",
                "image" => "",
                "hobby" => "",
                "education" => "",
                "working_evergy_since" => "",
                "working_in_the_sector_since" => "",
                "language" => "",
                "quote" => "",
                "quote_translated" => "",
            );

            if (get_field('type', $slideID) == 'quote') {
                $expertPhotos = get_field('images', get_field('expert', $slideID));


                // push data to popup arr
                $popupArr['number'] = $sliderCount;
                $popupArr['name'] = get_field('name', get_field('expert', $slideID));
                $popupArr['image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                $popupArr['hobby'] = get_field('hobby', get_field('expert', $slideID));
                $popupArr['education'] = get_field('education', get_field('expert', $slideID));
                $popupArr['working_evergy_since'] = get_field('working_evergy_since', get_field('expert', $slideID));
                $popupArr['working_in_the_sector_since'] = get_field('working_in_the_sector_since', get_field('expert', $slideID));
                $popupArr['language'] = get_field('language', get_field('expert', $slideID));
                $popupArr['quote'] = get_field('quote', $slideID);
                $popupArr['quote_translated'] = get_field('translation', $slideID);


                // push data to slide arr
                $slideArr['slide-image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                $slideArr['slide-image-caption-1'] = get_field('name', get_field('expert', $slideID));
                $slideArr['slide-image-caption-2'] = get_field('hobby', get_field('expert', $slideID));
                $slideArr['slide-image-caption-2-1'] = get_field('working_evergy_since', get_field('expert', $slideID));
                $slideArr['slide-text-1'] = get_field('quote', $slideID);
                $slideArr['slide-text-2'] = get_field('translation', $slideID);

                if (!empty(get_field('expert', $slideID))) {
                    if (empty(get_field('expert_card_button_title', $slideID))) {
                        $tempTitle = explode(" ", trim(get_field('name', get_field('expert', $slideID))));
                        $button1Title = "meet " . $tempTitle[0];
                    } else {
                        $button1Title = get_field('expert_card_button_title', $slideID);
                    }
                    $slideArr['slide-link-1'] = '<a href="#" data-quote-id="" data-modal-id="'.get_field('expert', $slideID).'" class="button button_primary" title="' . $button1Title . '"><span>' . $button1Title . '</span></a>';
                }

                if (!empty(get_field('case', $slideID))) {
                    if (empty(get_field('case_details_button_title', $slideID))) {
                        $button2Title = "explore " . get_field('column_4', get_field('case', $slideID));
                    } else {
                        $button2Title = get_field('case_details_button_title', $slideID);
                    }
                    $slideArr['slide-link-2'] = '<a href="' . get_permalink(get_field('case', $slideID)) . '" target="_blank" class="button button_primary" title="' . $button2Title . '"><span>' . $button2Title . '</span></a>';
                }
            } else if (get_field('type', $slideID) == 'news') {
                $slideArr['slide-image'] = get_field('image', $slideID);
                $slideArr['slide-image-caption-1'] = get_field('caption_below_image_bold', $slideID);
                $slideArr['slide-image-caption-2'] = get_field('caption_below_image_regular', $slideID);
                $slideArr['slide-text-1'] = get_field('headline', $slideID);
                $slideArr['slide-text-2'] = get_field('text_below_the_headline', $slideID);

                if (get_field('link1_type', $slideID) == "file" && !empty(get_field('file1', $slideID))) {
                    $slideArr['slide-link-1'] = '<a href="' . get_field('file1', $slideID) . '" target="_blank" class="button button_primary" title="' . get_field('button_title1', $slideID) . '"><span>' . get_field('button_title1', $slideID) . '</span></a>';
                } else if (get_field('link1_type', $slideID) == "url") {
                    $link1Url = get_field('link1', $slideID);
                    if (!empty($link1Url)) {
                        $slideArr['slide-link-1'] = '<a href="' . $link1Url['url'] . '" target="' . $link1Url['target'] . '" class="button button_primary" title="' . $link1Url['title'] . '"><span>' . $link1Url['title'] . '</span></a>';
                    }
                }

                if (get_field('link2_type', $slideID) == "file" && !empty(get_field('file2', $slideID))) {
                    $slideArr['slide-link-2'] = '<a href="' . get_field('file2', $slideID) . '" target="_blank" class="button button_primary" title="' . get_field('button_title2', $slideID) . '"><span>' . get_field('button_title2', $slideID) . '</span></a>';
                } else if (get_field('link2_type', $slideID) == "url") {
                    $link2Url = get_field('link2', $slideID);
                    if (!empty($link2Url)) {
                        $slideArr['slide-link-2'] = '<a href="' . $link2Url['url'] . '" target="' . $link2Url['target'] . '" class="button button_primary" title="' . $link2Url['title'] . '"><span>' . $link2Url['title'] . '</span></a>';
                    }
                }
            }

            $slidesArr[] = $slideArr;
            $popupsArr[] = $popupArr;
        }
        ?>

        <?php foreach ($slidesArr AS $slide) { ?>

            <div class="quote-slide swiper-slide">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-dl-16 col-tl-push-1 col-ml-15">
                            <div class="quote-slide__content">
                                <figure class="quote-slide__pic">
                                    <div class="quote-slide__img">
                                        <?php
                                        //echo qbai_get_image($slide['slide-image'], 'slider_adapt', false); 
                                        $sliderImage = wp_get_attachment_image_src($slide['slide-image'], 'full');
                                        ?>
                                        <img src="<?php echo $sliderImage[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">

                                    </div>
                                    <?php if (!empty($slide['slide-image-caption-1']) && !empty($slide['slide-image-caption-1'])) { ?>
                                        <figcaption>
                                            <?php if (!empty($slide['slide-image-caption-1'])) { ?>
                                                <span class="quote-slide__name"><?php echo $slide['slide-image-caption-1']; ?></span>
                                            <?php } ?>
                                            <?php if (!empty($slide['slide-image-caption-2'])) { ?>
                                                <span class="quote-slide__desc"><?php
                                                    echo trim($slide['slide-image-caption-2']);
                                                    if (!empty($slide['slide-image-caption-2-1'])) {
                                                        echo ", " . $slide['slide-image-caption-2-1'];
                                                    }
                                                    ?></span>
                                            <?php } ?>
                                        </figcaption>
                                    <?php } ?>
                                </figure>
                                <div class="quote-slide__in">
                                    <blockquote class="quote-slide__quote"><?php echo $slide['slide-text-1']; ?></blockquote>
                                    <div class="quote-slide__msg"><?php echo $slide['slide-text-2']; ?></div>
                                    <?php echo $slide['slide-link-1'] . $slide['slide-link-2']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
    <button class="quote-slider__arrow quote-slider__arrow_prev js-quote-prev">
        <svg class="icon icon-prev">
        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-prev"></use>
        </svg>
    </button>
    <button class="quote-slider__arrow quote-slider__arrow_next js-quote-next">
        <svg class="icon icon-next">
        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-next"></use>
        </svg>
    </button>
    <div class="swiper-pagination"></div>
</div>