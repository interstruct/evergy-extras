<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php

    if (!empty(get_field('map_image'))) {
        $header_map_land = wp_get_attachment_image_src(get_field('map_image'), 'full');
    } else {
        $header_map_land = wp_get_attachment_image_src(get_field('case_map_fallback_image', 'option'), 'full');
    }
    if (!empty(get_field('map_image_port'))) {
        $header_map_port = wp_get_attachment_image_src(get_field('map_image_port'), 'full');
    } else {
        $header_map_port = wp_get_attachment_image_src(get_field('case_map_fallback_image_portrait', 'option'), 'full');
    }
    ?>

    <main class="main js-main">
        <div class="case">
            <div class="case__wrapper ">
                <a href="<?php echo get_the_ID() . "/?pdf=" . get_the_ID(); ?>" class="case__download" download title="download case">
                    <span>download<br/>case</span>
                    <i>
                        <svg class="icon icon-icon-download-general">
                            <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-icon-download-general"></use>
                        </svg>
                    </i>
                </a>
                <div class="case__hero">
                    <div class="case__hero-bg case__hero-bg--landscape"
                         style="background-image: url('<?php echo $header_map_land[0]; ?>');"></div>
                    <div class="case__hero-bg case__hero-bg--portrait"
                         style="background-image: url('<?php echo $header_map_port[0]; ?>');"></div>
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-dl-17 col-dl-push-1 col-tl-push-1 col-tl-15">
                                <h1><?php the_field('column_13'); ?></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="case__content">
                    <div class="wrapper">
                        <div class="row">
                            <div class="case__content-wrapper col-dl-10 col-dl-push-4 col-dm-11 col-tl-15  col-ts-push-4 col-ts-15 col-ml-push-1 col-ml-15">
                                <div class="case__inner">
                                    <h2 class="case__title"><?php the_field('column_14'); ?></h2>
                                    <div class="text">
                                        <p>
                                            <strong><?php the_field('column_16'); ?></strong>
                                        </p>
                                        <ul>
                                            <?php
                                            $theList = preg_split('/\r\n|\r|\n/', get_field('column_17'));

                                            foreach ($theList as $theItem) {
                                                if ($theItem != "")
                                                    echo "<li>" . $theItem . "</li>";
                                            }
                                            ?>
                                        </ul>
                                        <p>
                                            <strong><?php the_field('column_18'); ?></strong>
                                        </p>
                                        <ul>
                                            <?php
                                            $theList = preg_split('/\r\n|\r|\n/', get_field('column_19'));

                                            foreach ($theList as $theItem) {
                                                if ($theItem != "")
                                                    echo "<li>" . $theItem . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="case__sidebar-wrapper col-dl-5 col-dl-push-2 col-dm-push-1 col-dm-5 col-tl-push-4 col-tl-15 col-ts-push-4 col-ts-15 col-ml-push-1 col-ml-15">
                                <div class="case__sidebar">
                                    <h2 class="case__title case__title_mob"><?php the_field('column_14'); ?></h2>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Country</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_5'); ?></p>
                                    </div>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Year of Mandate</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_7'); ?></p>
                                    </div>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Year of Commisioning</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_12'); ?></p>
                                    </div>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Key Technical Data</strong>
                                        </p>
                                        <div class="case__block-text">
                                            <ul>
                                                <?php
                                                $theList = preg_split('/\r\n|\r|\n/', get_field('column_15'));

                                                foreach ($theList as $theItem) {
                                                    if ($theItem != "")
                                                        echo "<li>" . $theItem . "</li>";
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="quote-slider-automatic">
            <div class="quote-slider__in swiper-container js-quote-slider">
                <div class="swiper-wrapper">
                    <?php
                    $args = array(
                        'post_type' => 'quotes_and_news',
                        'orderby' => 'rand',
                        'meta_query' => array(
                            array(
                                'key' => 'case',
                                'value' => get_the_ID(),
                                'compare' => '=',
                            )
                        ),
                        'fields' => 'ids',
                        'posts_per_page' => 1,
                    );
                    $slidesIDs = new WP_Query($args);

                    wp_reset_query();

                    $slidesArr = array();
                    $popupsArr = array();
                    $sliderCount = 0;
                    foreach ($slidesIDs->posts AS $slideID) {
                        $sliderCount++;
                        $slideArr = array(
                            "slide-image" => "",
                            "slide-image-caption-1" => "",
                            "slide-image-caption-2" => "",
                            "slide-image-caption-2-1" => "",
                            "slide-text-1" => "",
                            "slide-text-2" => "",
                            "slide-link-1" => "",
                            "slide-link-2" => "",
                        );

                        $popupArr = array(
                            "number" => 1,
                            "name" => "",
                            "image" => "",
                            "hobby" => "",
                            "education" => "",
                            "working_evergy_since" => "",
                            "working_in_the_sector_since" => "",
                            "language" => "",
                            "quote" => "",
                            "quote_translated" => "",
                        );

                        if (get_field('type', $slideID) == 'quote') {
                            $expertPhotos = get_field('images', get_field('expert', $slideID));

                            // push data to popup arr
                            $popupArr['number'] = $sliderCount;
                            $popupArr['name'] = get_field('name', get_field('expert', $slideID));
                            $popupArr['image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                            $popupArr['hobby'] = get_field('hobby', get_field('expert', $slideID));
                            $popupArr['education'] = get_field('education', get_field('expert', $slideID));
                            $popupArr['working_evergy_since'] = get_field('working_evergy_since', get_field('expert', $slideID));
                            $popupArr['working_in_the_sector_since'] = get_field('working_in_the_sector_since', get_field('expert', $slideID));
                            $popupArr['language'] = get_field('language', get_field('expert', $slideID));
                            $popupArr['quote'] = get_field('quote', $slideID);
                            $popupArr['quote_translated'] = get_field('translation', $slideID);


                            // push data to slide arr
                            $slideArr['slide-image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                            $slideArr['slide-image-caption-1'] = get_field('name', get_field('expert', $slideID));
                            $slideArr['slide-image-caption-2'] = get_field('hobby', get_field('expert', $slideID));
                            $slideArr['slide-image-caption-2-1'] = get_field('working_evergy_since', get_field('expert', $slideID));
                            $slideArr['slide-text-1'] = get_field('quote', $slideID);
                            $slideArr['slide-text-2'] = get_field('translation', $slideID);

                            if (!empty(get_field('expert', $slideID))) {
                                if (empty(get_field('expert_card_button_title', $slideID))) {
                                    $tempTitle = explode(" ", trim(get_field('name', get_field('expert', $slideID))));
                                    $button1Title = "meet " . $tempTitle[0];
                                } else {
                                    $button1Title = get_field('expert_card_button_title', $slideID);
                                }
                                $slideArr['slide-link-1'] = '<a href="#" data-quote-id="" data-modal-id="' . get_field('expert', $slideID) . '" class="button button_primary" title="'.$button1Title.'"><span>' . $button1Title . '</span></a>';
                            }

                            if (!empty(get_field('case', $slideID))) {
                                $slideArr['slide-link-2'] = '<a href="' . get_permalink(get_field('case', $slideID)) . '" target="_blank" title="' . get_field('case_details_button_title', $slideID) . '" class="button button_primary"><span>' . get_field('case_details_button_title', $slideID) . '</span></a>';
                            }
                        } else if (get_field('type', $slideID) == 'news') {
                            $slideArr['slide-image'] = get_field('image', $slideID);
                            $slideArr['slide-image-caption-1'] = get_field('caption_below_image_bold', $slideID);
                            $slideArr['slide-image-caption-2'] = get_field('caption_below_image_regular', $slideID);
                            $slideArr['slide-text-1'] = get_field('headline', $slideID);
                            $slideArr['slide-text-2'] = get_field('text_below_the_headline', $slideID);

                            if (get_field('link1_type', $slideID) == "file" && !empty(get_field('file1', $slideID))) {
                                $slideArr['slide-link-1'] = '<a href="' . get_field('file1', $slideID) . '" targe="_blank" class="button button_primary" title="' . get_field('button_title1', $slideID) . '"><span>' . get_field('button_title1', $slideID) . '</span></a>';
                            } else if (get_field('link1_type', $slideID) == "url") {
                                $link1Url = get_field('link1', $slideID);
                                if (!empty($link1Url)) {
                                    $slideArr['slide-link-1'] = '<a href="' . $link1Url['url'] . '" target="' . $link1Url['target'] . '" class="button button_primary" title="' . $link1Url['title'] . '"><span>' . $link1Url['title'] . '</span></a>';
                                }
                            }

                            if (get_field('link2_type', $slideID) == "file" && !empty(get_field('file2', $slideID))) {
                                $slideArr['slide-link-2'] = '<a href="' . get_field('file2', $slideID) . '" targe="_blank" class="button button_primary" title="' . get_field('button_title2', $slideID) . '"><span>' . get_field('button_title2', $slideID) . '</span></a>';
                            } else if (get_field('link2_type', $slideID) == "url") {
                                $link2Url = get_field('link2', $slideID);
                                if (!empty($link2Url)) {
                                    $slideArr['slide-link-2'] = '<a href="' . $link2Url['url'] . '" target="' . $link2Url['target'] . '" class="button button_primary" title="' . $link2Url['title'] . '"><span>' . $link2Url['title'] . '</span></a>';
                                }
                            }
                        }

                        $slidesArr[] = $slideArr;
                        $popupsArr[] = $popupArr;
                    }
                    ?>

                    <?php foreach ($slidesArr AS $slide) { ?>

                        <div class="quote-slide swiper-slide">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-dl-16 col-tl-push-1 col-ml-15">
                                        <div class="quote-slide__content">
                                            <figure class="quote-slide__pic">
                                                <div class="quote-slide__img">
                                                    <?php
                                                    $sliderImage = wp_get_attachment_image_src($slide['slide-image'], 'full');
                                                    ?>
                                                    <img src="<?php echo $sliderImage[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">

                                                </div>
                                                <?php if (!empty($slide['slide-image-caption-1']) && !empty($slide['slide-image-caption-1'])) { ?>
                                                    <figcaption>
                                                        <?php if (!empty($slide['slide-image-caption-1'])) { ?>
                                                            <span class="quote-slide__name"><?php echo $slide['slide-image-caption-1']; ?></span>
                                                        <?php } ?>
                                                        <?php if (!empty($slide['slide-image-caption-2'])) { ?>
                                                            <span class="quote-slide__desc"><?php
                                                                echo trim($slide['slide-image-caption-2']);
                                                                if (!empty($slide['slide-image-caption-2-1'])) {
                                                                    echo ", " . $slide['slide-image-caption-2-1'];
                                                                }
                                                                ?></span>
                                                        <?php } ?>
                                                    </figcaption>
                                                <?php } ?>
                                            </figure>
                                            <div class="quote-slide__in">
                                                <blockquote
                                                        class="quote-slide__quote"><?php echo $slide['slide-text-1']; ?></blockquote>
                                                <div class="quote-slide__msg"><?php echo $slide['slide-text-2']; ?></div>
                                                <?php echo $slide['slide-link-1']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>
                <button class="quote-slider__arrow quote-slider__arrow_prev js-quote-prev">
                    <svg class="icon icon-prev">
                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-prev"></use>
                    </svg>
                </button>
                <button class="quote-slider__arrow quote-slider__arrow_next js-quote-next">
                    <svg class="icon icon-next">
                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-next"></use>
                    </svg>
                </button>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <div class="projects">
            <div class="projects-pattern js-projects is-simple-projects"
                 data-json="<?php echo get_stylesheet_directory_uri(); ?>/assets/projects.json" data-wp-request="true">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-dl-12 col-dl-push-4 col-ds-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
                            <div class="projects-pattern__content">
                                <h3><?php the_sub_field('headline'); ?></h3>
                                <p><?php the_sub_field('copy_text'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="projects-pattern__filter col-dl-4 col-dl-push-1 col-ml-16 col-ml-push-0">
                            <div class="filter-box js-filter-box">
                                <div class="filter-box__col">
                                    <div class="filter-box__name">filter<br/>cases<br/>by</div>
                                    <div class="filter-box__list">
                                        <a href="#" class="filter-box__case js-filter-btn <?php echo get_field('column_6') == 'PV' ? "is-active" : ""; ?>" title="Show PV" data-filter-type="PV">
                                            <svg class="icon icon-filter-pv">
                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-pv"></use>
                                            </svg>
                                        </a>
                                        <a href="#" class="filter-box__case js-filter-btn  <?php echo get_field('column_6') == 'Wind' ? "is-active" : ""; ?>" title="Show Wind" data-filter-type="Wind">
                                            <svg class="icon icon-filter-w">
                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-w"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                                <div class="filter-box__col">
                                    <div class="filter-box__name">download<br/>track<br/>record</div>
                                    <div class="filter-box__list">
                                        <a href="<?php echo get_option('pv-pdf-url'); ?>" class="filter-box__load"
                                           download title="Download PV track record">
                                            <svg class="icon icon-download-pv">
                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-pv"></use>
                                            </svg>
                                        </a>
                                        <a href="<?php echo get_option('wind-pdf-url'); ?>" class="filter-box__load"
                                           download title="Download Wind track record">
                                            <svg class="icon icon-download-w">
                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-w"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-dl-12 col-ml-15 col-ml-push-1">
                            <div class="projects-pattern__list-wrapper js-projects-list-wrapper">
                                <div class="projects-pattern__list js-projects-list" data-active-project="<?php echo get_the_ID(); ?>">
                                    <?php

                                    $projects_first_package = projectjson_callback("yes", -1, get_field('column_6'));
                                    $load_more_btn = "";
                                    foreach ($projects_first_package as $item) {
                                        if ($load_more_btn == "" && $item['last'] != 0) {
                                            $load_more_btn = 'hidden';
                                        }
                                        if ($item['type'] != 'spec') {
                                            ?>
                                            <div class="projects-pattern__item"
                                                 style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                <a href="<?php echo $item['url']; ?>" class="project <?php echo $item['isActive'] == 'true' ? "is-active" : ""; ?> js-projects-storage-btn" title="Read more about <?php echo $item['title']; ?>">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <?php if ($item['type'] == 'Wind') { ?>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 322 177"
                                                         style="enable-background:new 0 0 322 177;"
                                                         xml:space="preserve">
            <g id="icons_x2F_cases_x2F_w">
            	<path id="W" d="M64.8,140H52.4L26.8,41.6h9.5l9.2,36.5l13.3,52.7c6.5-23.3,11.5-40.8,15-52.7l10.2-35h10.7l10.9,35.2l16.4,53.1
            		c5.4-24.9,9.2-42.6,11.5-53l8.2-36.8h9.1L128.5,140H116l-9.8-31.1l-16.6-54c-3.7,13.4-8.9,31.4-15.6,54L64.8,140z"></path>
            </g>
            </svg>
                                                <?php } else if ($item['type'] == 'PV') { ?>
                                                    <!--?xml version="1.0" encoding="utf-8"?-->
                                                    <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 322 177" enable-background="new 0 0 322 177"
                                                         xml:space="preserve">
            <g id="icons_x2F_cases_x2F_pv">
            	<path id="PV" d="M57.9,102.6H41.7V140h-9.1V41.6h23.7c11.5,0,20.5,2.9,27.1,8.7s9.9,13.4,9.9,22.8c0,8.2-3.2,15.1-9.6,20.9
            		C77.3,99.8,68.7,102.6,57.9,102.6z M57.4,49.6H41.7v45H59c7.7,0,13.8-2,18.3-6.1c4.5-4.1,6.8-9.2,6.8-15.3c0-7.2-2.3-12.9-7-17.2
            		C72.4,51.7,65.8,49.6,57.4,49.6z M151.5,140h-12.4l-33.4-98.4h9.8l13,39l16.9,51.4l17.5-51.4l13.4-39h9.8L151.5,140z"></path>
            </g>
            </svg>
                                                <?php } ?>
                                            </span>
                                            <span class="project__text"><?php echo $item['desc']; ?></span>
                                            <span class="project__title"><?php echo $item['title']; ?></span>
                                            <div class="project__link-wrapper">
                                                <span class="project__link">Read more</span>
                                            </div>
                                        </span>
                                                </a>
                                            </div>
                                        <?php } else { ?>
                                            <div class="projects-pattern__item"
                                                 style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                <a href="<?php echo get_field('map_page', 'option'); ?>"
                                                   class="project project_special" title="Projects Map">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <img src="<?php echo $item['img']; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">
                                            </span>
                                            <span class="project__title project__title_special"><?php echo $item['title']; ?></span>
                                        </span>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-dl-16 col-dl-push-4 col-ml-15 col-ml-push-1">
                            <div class="projects-pattern__more-wrapper">
                                <button class="projects-pattern__more-btn <?php echo $load_more_btn; ?> js-more-btn">
                                    <svg class="icon icon-load-more">
                                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-load-more"></use>
                                    </svg>
                                    <strong>load more</strong>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php endwhile; // end of the loop.   ?>
<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>