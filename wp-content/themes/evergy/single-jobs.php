<?php get_header(); ?>
<body>
    <?php $header_logo = wp_get_attachment_image_src(get_field('shares_logo', 'option'), 'full'); ?>
    <img src="<?php echo $header_logo[0]; ?>" alt="<?php bloginfo('name'); wp_title(); ?>">
    <script>
        window.location.replace('<?php echo get_field('jobs_page', 'option'); ?>');
    </script>
</body>
<?php get_footer(); ?>