</div>
<div class="expert-details js-modal" data-wp-request="true">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-16 ">
                <div class="expert-details__in">
                    <div class="expert-details__content js-ps-scroll">
                        <div class="expert js-modal-content-wrapper">
                        </div>
                        <button class="expert-details__close js-modal-close">
                            <svg class="icon icon-close-big">
                            <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-close-big"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>