<?php
/**
 * dkpdf-index.php
 * This template is used to display the content in the PDF
 *
 * Do not edit this template directly,
 * copy this template and paste in your theme inside a directory named dkpdf
 */
?>
<?php
if (get_post_type('') == 'projects'){
    include 'case_template.php';
} else if (get_post_type('') == 'projects_pdf'){
    include 'projects_template.php';
}
?>