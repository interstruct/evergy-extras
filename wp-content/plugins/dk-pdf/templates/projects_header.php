<?php

$projectsType = get_the_title();
$pdfTitle = "";
if ($projectsType == 'PV') {
    $pdfTitle = get_field('title_for_pv_projects_pdf', 'option');
} else if ($projectsType == 'Wind') {
    $pdfTitle = get_field('title_for_wind_projects_pdf', 'option');
}
?>

<!-- BEGIN content -->
<div class="out">
    <div class="p-header ">
        <div class="p-header__text"> <?php echo $pdfTitle; ?> </div>
        <div class="p-header__logo">
            <?php $header_logo = wp_get_attachment_image_src(get_field('pdf_logo', 'option'), 'full'); ?>
            <img src="<?php echo $header_logo[0]; ?>" alt="" />
        </div>
        <div class="clearfix"></div>
    </div>
    <table class="p-table">
        <tr class="p-table__row p-table__row--head">
            <th class="p-table__cell p-table__cell--first" width="<?php echo $projectsType == 'PV' ? 145 : 144; ?>">Project</th>
            <th width="65">Evergy Mandate</th>
            <th width="85">Country</th>
            <th width="75">Installed Capacity in MW</th>
            <?php
            if ($projectsType == 'PV') {
                $colspan = 4;
                ?>
                <th width="78">Type of Installation</th>
                <th width="85">Module Manufacturer</th>
                <th width="88">Inverter Manufacturer</th>
                <?php
            } else if ($projectsType == 'Wind') {
                $colspan = 3;
                ?>
                <th width="78">N of Turbines</th>
                <th width="180">Type(s) of Turbine</th>
            <?php } ?>
            <th class="p-table__cell p-table__cell--last">Evergy Scope of Work</th>
        </tr>
    </table>
</div>