<?php 
/**
* dkpdf-header.php
* This template is used to display content in PDF Header
*
* Do not edit this template directly, 
* copy this template and paste in your theme inside a directory named dkpdf 
*/ 
?>

<?php 
  	global $post;
  	$pdf_header_image = sanitize_option( 'dkpdf_pdf_header_image', get_option( 'dkpdf_pdf_header_image' ) );
    $pdf_header_image_attachment = wp_get_attachment_image_src( $pdf_header_image, 'full' );
    $pdf_header_show_title = sanitize_option( 'dkpdf_pdf_header_show_title', get_option( 'dkpdf_pdf_header_show_title' ) );
    $pdf_header_show_pagination = sanitize_option( 'dkpdf_pdf_header_show_pagination', get_option( 'dkpdf_pdf_header_show_pagination' ) );
?>

<?php
if (get_post_type() == 'projects_pdf'){
    include 'projects_header.php';
}
?>